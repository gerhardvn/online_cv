# My Online CV

If you want to know more about me / hire me.

Build with [Jekyll](https://jekyllrb.com/)
* `bundle exec jekyll build` - One off build.
* `bundle exec jekyll serve` - Serve to test.

Hosted with [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/)
[See it here](https://gerhardvn.gitlab.io/online_cv/)
